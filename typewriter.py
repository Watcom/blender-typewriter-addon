# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# <pep8-80 compliant>
import bpy
from bpy.app.handlers import persistent
from bpy_extras.io_utils import ExportHelper
import random

# setup optional midi output

use_midi = True
try:
    from midiutil import MIDIFile
except ImportError:
    use_midi = False
    print("typewriter addon: disabling MIDI output, no midiutil module found")

bl_info = {
    'name': 'Typewriter Text',
    'description': 'Typewriter Text effect for font objects',
    'author': 'Bassam Kurdali, Vilem Novak, Jimmy Berry',
    'version': (0, 4, 1),
    'blender': (2, 7, 0),
    'location': 'Properties Editor, Text Context',
    'url': 'https://github.com/boombatower/blender-typewriter-addon',
    'category': 'Text'}

def randomize(t,width):
    nt=''
    lines=t.splitlines()
    totlen=len(t)
    i=0
    for l in lines:
        for ch in l:
            if i>totlen+1-width:
                nt+=random.choice(l)
            else:
                nt+=ch
            i+=1
        nt+='\n'
        i+=2
    return(nt)


def uptext(text):
    '''
    slice the source text up to the character_count
    '''
    source = text.source_text
    if source in bpy.data.texts:
        if text.separator!='':    strings=bpy.data.texts[source].as_string().split(text.separator)
        else:
            strings=[bpy.data.texts[source].as_string()]
        idx=min(len(strings),text.text_index)
        t=strings[idx]

        #remove line endings after separator
        while t.find('\n')==0:
            t=t[1:]
    else:
        t = source

    skip_set = set()
    if text.skip_spaces:
        skip_set.add(' ')
    if text.skip_newlines:
        skip_set.add('\n')
    if skip_set:
        word_map = [0]
        i = 0
        for ch in t:
            i = i + 1
            if ch not in skip_set:
                word_map.append(i)
        true_count = word_map[min(text.character_count, len(word_map) - 1)]
    else:
        true_count = text.character_count

    #randomize
    if text.use_randomize and len(t) > true_count:
        t=randomize(t[:true_count],text.randomize_width)

    prefix = ''
    if text.preserve_newline and text.character_start > 0:
        prefix = '\n' * t.count('\n', 0, text.character_start)
    if text.preserve_space and text.character_start > 0:
        prefix += ' ' * (text.character_start - (t.rfind('\n', 0, text.character_start) + 1))
    suffix = text.cursor_text if text.draw_cursor else ''
    t = t[text.character_start:true_count]
    if text.pause_string:
        t = t.replace(text.pause_string, '')

    text.body = prefix + t + suffix
    if use_midi:
        text.true_character_count_cache = len(prefix) + len(t.rstrip(' \n'))

@persistent
def typewriter_text_update_frame(scene):
    '''
    sadly we need this for frame change updating
    '''
    for text in scene.objects:
        if text.type == 'FONT' and text.data.use_animated_text:
            uptext(text.data)


def update_func(self, context):
    '''
    updates when changing the value
    '''
    uptext(self)


class ExportMidi(bpy.types.Operator, ExportHelper):
    '''
    export a midi file from the typewriter data
    '''
    bl_idname = "object.export_midi"
    bl_label = "Export MIDI file"
    bl_description = "Exports a MIDI file from a typewriter instance"
    filepath = bpy.props.StringProperty(subtype = "FILE_PATH")
    filename_ext = ".mid"
    filter_glob = bpy.props.StringProperty(default = "*.mid",
            options = {'HIDDEN'})
    tempo = bpy.props.IntProperty(name="Tempo", min = 10, default = 120)
    volume = bpy.props.IntProperty(name="Volume", min = 1, max = 127,
            default = 64)
    channel = bpy.props.IntProperty(name="Channel", min = 1, max = 15,
            default = 1)
    pitch = bpy.props.IntProperty(name="Pitch", min = 1, max = 127,
            default = 70)
    duration = bpy.props.IntProperty(name="Duration",
            description="Duration of each note, in beats / 64", min = 1,
            default = 5)
    mingap = bpy.props.IntProperty(name="Min Gap",
            description="Minimum gap between notes, in beats / 64", min = 0,
            default = 8)

    @classmethod
    def poll(cls, context):
        return context.active_object and context.active_object.type == 'FONT'
    def execute(self, context):
        if not use_midi:
            return {'CANCELLED'}
        bpy.ops.file.make_paths_absolute()

        scene = bpy.context.scene
        startFrame = scene.frame_start
        endFrame = scene.frame_end

        print("exporting MIDI file " + self.filepath + " from frame " +
                str(startFrame) + " to " + str(endFrame) + "...")
        track = 0
        time = 0
        frames_per_beat = (scene.render.fps /
                scene.render.fps_base) * 60 / self.tempo
        midifile = MIDIFile(1)
        midifile.addTempo(track, time, self.tempo)
        cur_frame = scene.frame_current  # save current frame
        prev_count = 0
        ob = context.active_object
        frames = [ ]
        def beats_to_frames(x):
            return x * frames_per_beat
        def frames_to_beats(x):
            return x / frames_per_beat
        mingap = beats_to_frames(self.mingap / 64)
        for f in range(scene.frame_start, scene.frame_end + 1):
            scene.frame_set(f)
            cur_count = ob.data.true_character_count_cache
            if cur_count != prev_count:
                prev_count = cur_count
                if frames:
                    if f <= frames[-1]:
                        continue
                    if (f - frames[-1]) < mingap:
                        f = frames[-1] + mingap
                frames.append(f)
        scene.frame_set(cur_frame) # restore
        if not frames:
            return {'CANCELLED'}
        #start_frame = frames[0]
        start_frame = scene.frame_start
        for f in frames:
            beats = frames_to_beats(f - start_frame)
            midifile.addNote(track, self.channel - 1, self.pitch, time + beats,
                    self.duration / 64, self.volume)
        with open(self.filepath, "wb") as output_file:
            midifile.writeFile(output_file)
        return {'FINISHED'}


class TEXT_PT_Typewriter(bpy.types.Panel):
    '''
    Typewriter Effect Panel
    '''
    bl_label = "Typewriter Effect"
    bl_idname = "TEXT_PT_Typewriter"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = 'data'

    @classmethod
    def poll(cls, context):
        return context.active_object and context.active_object.type == 'FONT'

    def draw_header(self, context):
        text = context.active_object.data
        layout = self.layout
        layout.prop(text, 'use_animated_text', text="")

    def draw(self, context):
        st = context.space_data
        text = context.active_object.data
        layout = self.layout
        if use_midi:
            split = layout.split()
            col = split.column();
            col.operator("object.export_midi",
                    text="Export MIDI file", icon='FILE_SOUND')
        layout.prop(text,'character_start')
        layout.prop(text,'draw_cursor')
        if text.draw_cursor:
            layout.prop(text,'cursor_text')
        layout.prop(text,'pause_string')
        layout.prop(text,'skip_spaces')
        layout.prop(text,'skip_newlines')
        layout.prop(text,'preserve_newline')
        layout.prop(text,'preserve_space')
        layout.prop(text,'character_count')
        layout.prop(text,'source_text')
        if text.source_text in bpy.data.texts:
            layout.prop(text,'separator')
            layout.prop(text,'text_index')
        layout.prop(text,'use_randomize')
        if text.use_randomize:
            layout.prop(text,'randomize_width')

def register():
    '''
    addon registration function
    '''
    # register the module:
    bpy.utils.register_module(__name__)
    # create properties
    bpy.types.TextCurve.character_start = bpy.props.IntProperty(
      name="character_start",update=update_func, min=0, options={'ANIMATABLE'})
    bpy.types.TextCurve.preserve_newline = bpy.props.BoolProperty(
      name="preserve_newline", default=True)
    bpy.types.TextCurve.preserve_space = bpy.props.BoolProperty(
      name="preserve_space", default=True)
    bpy.types.TextCurve.character_count = bpy.props.IntProperty(
      name="character_count",update=update_func, min=0, options={'ANIMATABLE'})
    bpy.types.TextCurve.true_character_count_cache = bpy.props.IntProperty(
      name="true_character_count_cache")
    bpy.types.TextCurve.backup_text = bpy.props.StringProperty(
      name="backup_text")
    bpy.types.TextCurve.use_animated_text = bpy.props.BoolProperty(
      name="use_animated_text", default=False)
    bpy.types.TextCurve.draw_cursor = bpy.props.BoolProperty(
      name="Draw cursor", default=False)
    bpy.types.TextCurve.cursor_text = bpy.props.StringProperty(
      name="Cursor text", default='█')
    bpy.types.TextCurve.pause_string = bpy.props.StringProperty(
      name="Pause string", description="String to be removed from text, " +
      "but causes a one character delay", default='`')
    bpy.types.TextCurve.skip_spaces = bpy.props.BoolProperty(
      name="Skip spaces", description="Skip spaces when animating",
      default=False)
    bpy.types.TextCurve.skip_newlines = bpy.props.BoolProperty(
      name="Skip newlines", description="Skip newlines when animating",
      default=False)
    bpy.types.TextCurve.source_text = bpy.props.StringProperty(
      name="source_text")


    bpy.types.TextCurve.text_index = bpy.props.IntProperty(
      name="index",update=update_func, min=0, options={'ANIMATABLE'})
    bpy.types.TextCurve.separator = bpy.props.StringProperty(
      name="separator", default='#' )
    bpy.types.TextCurve.use_randomize = bpy.props.BoolProperty(
      name="randomize", default=False)
    bpy.types.TextCurve.randomize_width = bpy.props.IntProperty(
      name="randomize width",update=update_func, default=10, min=0, options={'ANIMATABLE'})

    # add the frame change handler
    bpy.app.handlers.frame_change_pre.append(typewriter_text_update_frame)


def unregister():
    '''
    addon unregistration function
    '''
    # remove the frame change handler
    bpy.app.handlers.frame_change_pre.remove(typewriter_text_update_frame)

    # remove the properties
    # XXX but how???
    # remove the panel
    bpy.utils.unregister_module(__name__)

if __name__ == "__main__":
    register()
